from django.shortcuts import render
from .serializers import BookmarkSerializer
from .models import Bookmark
from rest_framework import viewsets

# Create your views here.
class BookmarkViewset(viewsets.ModelViewSet):
    queryset = Bookmark.objects.all()
    serializer_class = BookmarkSerializer
