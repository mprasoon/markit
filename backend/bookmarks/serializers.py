from .models import Bookmark
from rest_framework import serializers

class BookmarkSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bookmark
        fields = ('id','title','link','notes','created_by','categories','liked_by')
