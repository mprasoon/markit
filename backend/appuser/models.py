from django.db import models
from django.contrib.auth.models import User

class Base(models.Model):

    marked_for_deletion = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract: True

class AppUser(Base):
    """
    Custom user model
    :field: user: OneToOneField to the user
    :field: created_at: What time the model was created at
    :field: marked_for_deletion: If this user is marked for deletion
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
