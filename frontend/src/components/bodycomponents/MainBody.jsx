import React, { Component } from "react";
import Header from './Header'
import Bookmark from '../subcomponents/Bookmark'

class MainBody extends Component {
        state = {
      bookmarksList : this.props.bookmarks,
      appUsers: this.props.appUsers
    };
    returnBookmarksList = () => {
      return this.state.bookmarksList.map(bookmark => (<Bookmark bookmark={bookmark} appUser={this.state.appUsers[bookmark.created_by - 1]} />))
    }
    render () {
        return (
            <React.Fragment>
                <Header />
                <div className="container">
                  <div className="row">
                    <div className="col-lg-8 col-md-10 mx-auto">
                        {this.returnBookmarksList()}
                      <div className="clearfix">
                        <a className="btn btn-primary float-right" href="www.google.com">Older Posts &rarr;</a>
                      </div>
                    </div>
                  </div>
                </div>
            </React.Fragment>
         )
    }
}

export default MainBody;
